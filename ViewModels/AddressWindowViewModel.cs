﻿using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFCodeTest.Models;
using WPFCodeTest.Services;

namespace WPFCodeTest.ViewModels
{
    public class AddressWindowViewModel : MvxViewModel
    {
        private readonly GoogleMapsApiService _googleMapsApi;
        private readonly PinLocationService _pinLocationService;
        public AddressWindowViewModel()
        {
            _googleMapsApi = new GoogleMapsApiService();
            _pinLocationService = new PinLocationService();
        }
        public ObservableCollection<GooglePlaceAutoCompletePrediction> Places { get; set; }

        public IMvxCommand GetPlacesCommand { get; set; }
        public IMvxCommand GoMainPageCommand { get; set; }


        public async Task GetPlacesByName(string placeText)
        {
            try
            {
                var places = await _googleMapsApi.GetPlaces(placeText);
                var placeResult = places.AutoCompletePlaces;
                if (placeResult != null && placeResult.Count > 0)
                {
                    Places = new ObservableCollection<GooglePlaceAutoCompletePrediction>(placeResult);
                    await RaisePropertyChanged("Places");
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("GetPlacesByName err: " + ex.Message);
            }
        }

        public async Task GetPlacesDetail(GooglePlaceAutoCompletePrediction place)
        {
            var _place = await _googleMapsApi.GetPlaceDetails(place.PlaceId);
            if (place != null)
                _pinLocationService.SavePinLocationAsync(
                    new PinLocation
                    {
                        Address = _place.Name,
                        Latitude = _place.Latitude,
                        Longitude = _place.Longitude
                    });
            //await _navigationService.Navigate<MainWindowViewModel>();

        }
    }
}
