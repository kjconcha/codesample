﻿using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFCodeTest.Models;
using WPFCodeTest.Pages;
using WPFCodeTest.Services;

namespace WPFCodeTest.ViewModels
{
    public class MainWindowViewModel : MvxViewModel
    {
        //IPinLocationService _pinLocationService;
        //IMvxNavigationService _navigationService;

        PinLocationService PinLocationService;

        public MainWindowViewModel()
        {
            //_pinLocationService = Mvx.IoCProvider.Resolve<IPinLocationService>();
            //_navigationService = Mvx.IoCProvider.Resolve<IMvxNavigationService>();

            PinLocationService = new PinLocationService();

            AddressPageCommand = new MvxCommand(async () => await GotoAddressPage());
            ClearPinLocationsCommand = new MvxCommand(() => ClearPinLocations());

            PinsInit();
        }


        public async Task PinsInit()
        {
            PinLocations = new ObservableCollection<PinLocation>();
            var _pins = PinLocationService.GetPinLocations();
            if (_pins != null && _pins.Count > 0)
                PinLocations = new ObservableCollection<PinLocation>(_pins);

            await RaisePropertyChanged("PinLocations");
        }



        //public override Task Initialize()
        //{
        //    PinLocations = new ObservableCollection<PinLocation>();
        //    var _pins = PinLocationService.GetPinLocations();
        //    if (_pins != null && _pins.Count > 0)
        //        PinLocations = new ObservableCollection<PinLocation>(_pins);

        //    return base.Initialize();
        //}
        public IMvxCommand AddressPageCommand { get; set; }
        public IMvxCommand ClearPinLocationsCommand { get; set; }

        private async Task GotoAddressPage()
        {
            await Task.Delay(0);
            //await _navigationService.Navigate<AddressWindowViewModel>();

            var studWin = new AddressWindow();
            studWin.Show();
            studWin.Topmost = true;

        }

        private void ClearPinLocations()
        {
            PinLocationService.ClearPinLocationsAsync();
            PinLocations.Clear();
        }

        private ObservableCollection<PinLocation> pinLocations;
        public ObservableCollection<PinLocation> PinLocations
        {
            get => pinLocations;
            set
            {
                pinLocations = value;
            }
        }
    }
}
