using Newtonsoft.Json;
using Plugin.Settings;
using Plugin.Settings.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using WPFCodeTest.Models;

namespace WPFCodeTest.Helpers
{
    public static class Settings
    {
        private static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }

        #region Setting Constants

        private const string SettingsKey = "settings_key";
        private static readonly string SettingsDefault = string.Empty;
        const string pinLocationListKey = "pinLocationListKey";

        #endregion


        public static string GeneralSettings
        {
            get
            {
                return AppSettings.GetValueOrDefault(SettingsKey, SettingsDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(SettingsKey, value);
            }
        }



        public static List<PinLocation> PinLocations
        {
            get
            {
                string value = AppSettings.GetValueOrDefault(pinLocationListKey, string.Empty);
                List<PinLocation> pinLocations;
                if (string.IsNullOrEmpty(value))
                    pinLocations = new List<PinLocation>();
                else
                    pinLocations = JsonConvert.DeserializeObject<List<PinLocation>>(value);
                return pinLocations;
            }
            set
            {
                string pinLocationVal = JsonConvert.SerializeObject(value);
                AppSettings.AddOrUpdateValue(pinLocationListKey, pinLocationVal);
            }
        }

    }
}
