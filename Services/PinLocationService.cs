﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFCodeTest.Helpers;
using WPFCodeTest.Models;

namespace WPFCodeTest.Services
{
    public interface IPinLocationService
    {
        List<PinLocation> GetPinLocations();
        void SavePinLocationAsync(PinLocation pinLocation);
        void ClearPinLocationsAsync();
    }
    public class PinLocationService : IPinLocationService
    {
        public void ClearPinLocationsAsync()
        {
            // re - initialized
            Settings.PinLocations = new List<PinLocation>();
        }

        public List<PinLocation> GetPinLocations()
        {
            return Settings.PinLocations;
        }

        public void SavePinLocationAsync(PinLocation pinLocation)
        {
            var pinLocations = Settings.PinLocations;
            pinLocations.Add(pinLocation);
            Settings.PinLocations = pinLocations;
        }
    }
}
