﻿using MvvmCross;
using MvvmCross.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFCodeTest.Services;
using WPFCodeTest.Utils;

namespace WPFCodeTest
{
    public class FormsApp : MvxApplication
    {
        public override void Initialize()
        {
            GoogleMapsApiService.Initialize(Constants.GoogleMapsApiKey);
            Mvx.IoCProvider.RegisterType<IGoogleMapsApiService, GoogleMapsApiService>();
            Mvx.IoCProvider.RegisterType<IPinLocationService, PinLocationService>();

            System.Diagnostics.Debug.WriteLine("Initialize");

        }
    }
}
