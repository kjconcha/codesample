﻿using Microsoft.Maps.MapControl.WPF;
using MvvmCross;
using MvvmCross.Platforms.Wpf.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPFCodeTest.Services;
using WPFCodeTest.ViewModels;

namespace WPFCodeTest.Pages
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MvxWindow
    {
        MainWindowViewModel mainPageViewModel;
        public MainWindow()
        {
            this.DataContext = mainPageViewModel = new MainWindowViewModel();
            InitializeComponent();
        }



        protected override async void OnActivated(EventArgs e)
        {
            var pinLocationService = Mvx.IoCProvider.Resolve<IPinLocationService>();
            var pins = pinLocationService.GetPinLocations();
            await mainPageViewModel.PinsInit();

            foreach (var item in pins)
            {
                Pushpin m = new Pushpin();
                m.Location = new Location() { Latitude = item.Latitude, Longitude = item.Longitude };
                myMap.Children.Add(m);
            }

            base.OnActivated(e);
        }

        //private void MainPage_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        //{
        //    mainPageViewModel = e.NewValue as MainWindowViewModel;

        //    var pinLocationService = Mvx.IoCProvider.Resolve<IPinLocationService>();
        //    var pins = pinLocationService.GetPinLocations();

        //    foreach (var item in pins)
        //    {
        //        Pushpin m = new Pushpin();
        //        m.Location = new Location() { Latitude = item.Latitude, Longitude = item.Longitude };
        //        myMap.Children.Add(m);
        //    }
        //}

        private async void ClearData_Click(object sender, RoutedEventArgs e)
        {
            mainPageViewModel.ClearPinLocationsCommand.Execute();
            myMap.Children.Clear();
            await mainPageViewModel.PinsInit();
        }
    }
}
