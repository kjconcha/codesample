﻿using MvvmCross.Platforms.Wpf.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPFCodeTest.Models;
using WPFCodeTest.ViewModels;

namespace WPFCodeTest.Pages
{
    /// <summary>
    /// Interaction logic for AddressWindow.xaml
    /// </summary>
    public partial class AddressWindow : MvxWindow
    {
        AddressWindowViewModel viewModel;
        public AddressWindow()
        {
            this.DataContext = viewModel = new AddressWindowViewModel();
            InitializeComponent();

            qText.TextChanged += QText_TextChanged;

            this.DataContextChanged += AddressPage_DataContextChanged;
            AddressList.SelectionChanged += MyList_SelectionChanged;
        }


        private async void MyList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var lv = sender as ListView;
            var place = lv.SelectedItem as GooglePlaceAutoCompletePrediction;

            await viewModel.GetPlacesDetail(place);
            this.Close();
        }

        private void AddressPage_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            viewModel = e.NewValue as AddressWindowViewModel;
            
        }

        private async void QText_TextChanged(object sender, TextChangedEventArgs e)
        {
            var _m = sender as TextBox;

            System.Diagnostics.Debug.WriteLine("text>>> " + _m.Text );

            await viewModel.GetPlacesByName(_m.Text);
        }
    }
}
